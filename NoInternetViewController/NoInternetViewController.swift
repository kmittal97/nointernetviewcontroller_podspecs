//
//  NoInternetViewController.swift
//  NoInternetViewController
//
//  Created by Varenia CIMS on 10/26/18.
//  Copyright © 2018 Varenia CIMS. All rights reserved.
//

import UIKit
import Reachability

class NoInternetViewController: UIViewController {
    
    let reachability = Reachability()
    
    var isActive : Bool = true
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var noInternetView = NoInternetConnectionView.init(frame: CGRect.init(x: 0, y: 0, width:  UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isActive = true
        noInternetView.compHandler = {
            self.internetSettings()
            
        }
        
        do {
            try reachability.startNotifier()
        } catch let err as NSError {
            print(err.localizedDescription)
        }
        noInternetView.isOpaque = true
        networkObserver()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isActive = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        isActive = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func internetSettings() {
        print("Internet Settings Called")
        var alertController = UIAlertController (title: "Kenante", message: "Go to Settings?", preferredStyle: .alert)
        
        var settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            let settingsUrl = NSURL(string: UIApplication.openSettingsURLString + Bundle.main.bundleIdentifier!)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                //   UIApplication.shared.openURL(url as URL)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(settingsAction)
        alertController.addAction(cancelAction)
        
        
        present(alertController, animated: true, completion: nil)
    }
    
    func networkObserver() {
        
        reachability.reachableBlock = {
            (reachabity) in
            
            //   self.view.makeToast("Network Available")
            if self.isActive {
                self.networkReachable()
                self.noInternetView.removeFromSuperview()
            }
        }
        
        reachability.unreachableBlock = {
            (reachability) in
            if self.isActive {
                self.networkUnReachable()
                // self.view.makeToast("Network Not Available.")
                self.view.addSubview(self.noInternetView)
            }
        }
    }
    
    
    func networkReachable() {
        
    }
    
    func networkUnReachable() {
        
    }
}


class NoInternetConnectionView: UIView {
    
    @IBOutlet weak var noInternetLabel: UILabel!
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var pleaseCheckLbl: UILabel!
    var compHandler : (() -> ()) = { print("completion")  }
    //   @IBOutlet weak var noInternetLabel: UILabel!
    // @IBOutlet var noInternetView: UIView!
    //   @IBOutlet var noInternetView: UIView!
    
    //  @IBOutlet weak var noInternetLabel: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //    commonInit()
    }
    
    func commonInit() {
        let view  = Bundle.main.loadNibNamed("NoInternetView", owner: self, options: nil)?.first as! UIView
        
        view.frame = self.bounds
        view.layer.cornerRadius = 6.0 * UIScreen.main.bounds.height/568.0
        
        self.addSubview(view)
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(internetTapped))
        self.pleaseCheckLbl.isUserInteractionEnabled = true
        self.pleaseCheckLbl.addGestureRecognizer(tap)
        
        //self.noInternetLabel.font = UIFont.getRobotoRegular(size: 18)
        let attributedString = NSMutableAttributedString.init()
        let attribute = NSAttributedString.init(string: "Please check internet", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor : UIColor.black])
        let attribute1 = NSAttributedString.init(string: " settings", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor : UIColor.green])
        attributedString.append(attribute)
        attributedString.append(attribute1)
        self.pleaseCheckLbl.attributedText = attributedString
        cardView.layer.cornerRadius = 6.0 * UIScreen.main.bounds.height/568.0
        //       self.backgroundColor = UIColor.orange
        print(view)
        //        view.bounds = CGRect.init(x: 0, y: 0, width: 100, height: 100)
        //        addSubview(view)
        //        noInternetView.frame = self.bounds
        //        noInternetView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
    }
    
    
    
    @objc func internetTapped() {
        
        print("called")
        compHandler()
    }
}
